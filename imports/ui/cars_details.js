import './cars_details.html';
import {Cars, Ops} from '/imports/collections';

TemplateController('cars_details', {
    onCreated() {
        // Extragem id-ul din url. Numele parametrului este definit in /imports/routes.
        // In loc de :id ar fi putut fi orice, :moloz de ex, iar aici am fi folosit getParam('moloz')
        const _id = FlowRouter.getParam('id');
        // folosim id-ul ca parametru pentru functia de subscribe
        // Ne "abonam" astfel doar la datele care respecta filtrele respective
        this.subscribe('cars.find', {_id: _id});
        this.subscribe('ops.find', {carId: _id});
    },
    helpers: {
        car() {
            return Cars.findOne({_id: FlowRouter.getParam('id')});
        },
        ops() {
            return Ops.find({carId: FlowRouter.getParam('id')});
        },
        OpsCollection() {
            return Ops;
        }
    },
    events: {
        'click [data-remove]'(event) {
            const id = event.currentTarget.dataset.remove;
            Meteor.call('ops.remove', id);
        }
    }
});