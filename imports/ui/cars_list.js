import './cars_list.html';
import {Cars} from '/imports/collections';

/*
TemplateController este un obiect javascript asociat unui template in care poti sa definesti diferite functionalitati precum:
    - lifecycle hooks (onCreated, onRendered, onDestroyed) in care poti defini anumite behaviours care sa aiba loc la anumite momente in "viata" unui template;
    onCreated are loc cu foarte putin timp inainte ca templateul sa fie efectiv randat (onRendered) in browser
    - helpers este o colectie de functii cu care pasezi date la template. Fiecare functie din helpers poate fi apelata in template asa: {{ functie }}
    - events este o colectie de functii cu care prelucrezi anumite eventuri (click, keyUp, keyDown, blur, change, etc.) pe care le poti genera din orice element din html
    - state (nu il folosim aici) este un obiect in care stochezi variabile encapsulate in template-ul respectiv
*/
TemplateController('cars_list', {
    onCreated() {
        // inainte de a fi afisat template-ul in browser, te "abonezi" la data-publication-ul "cars.find";
        this.subscribe('cars.find');
    },
    helpers: {
        cars() {
            // imediat cum datele din subscription sunt disponibile, aceasta functie va intoarce toate datele pentru a fi folosite in template
            return Cars.find();
        },
        CarsCollection() {
            // Avem nevoie sa returnam clasa Cars pentru ca quickForm-ul sa poata sa genereze toate input-urile in baza schemei defininte in /imports/collections
            return Cars;
        }
    },
    events: {
        // [data-remove] este un selector CSS, putea fi .clasa sau #id sau orice alt selector valid in css. Identificam astfel elementul din HTML care trebuie sa
        // "reactioneze" la evenimentul "click".
        'click [data-remove]'(event) {
            // asa extragem data dintr-un data-attribute  https://developer.mozilla.org/en-US/docs/Learn/HTML/Howto/Use_data_attributes
            // in template am folosit data-remove="idulmasinii" ca sa stim ce masina sa stergem
            const id = event.currentTarget.dataset.remove;
            Meteor.call('cars.remove', id);
        }
    }
});