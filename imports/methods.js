import {Cars, Ops} from '/imports/collections';

Meteor.methods({
    'cars.insert'(doc) {
        Cars.insert(doc);
    },
    'cars.update'(doc, id) {
        Cars.update(id, {doc});
    },
    'cars.remove'(id) {
        Cars.remove(id);
        // Stergem si operatiunile asociatei unei masini, ca sa nu ramana obiecte "orfane" in baza de date
        Ops.remove({carId: id});
    },
    'ops.insert'(doc) {
        Ops.insert(doc);
    },
    'ops.update'(doc, id) {
        Ops.update(id, {doc});
    },
    'ops.remove'(id) {
        Ops.remove(id);
    }
});