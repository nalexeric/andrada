export const Cars = new Mongo.Collection('cars');

// asta este optional, folosim pentru validarea datelor in formular
Cars.attachSchema(new SimpleSchema({
    licensePlate: {
        type: String,
        label: 'Numar de inmatriculare'
    },
    make: {
        type: String,
        label: 'Marca auto'
    },
    model: {
        type: String,
        label: 'Model',
        optional: true
    }
}));

export const Ops = new Mongo.Collection('ops');

Ops.attachSchema(new SimpleSchema({
    type: {
        type: String,
        label: 'Tip operatiune',
        autoform: {
            options: [
                {label: 'Reparatie', value: 'reparatie'},
                {label: 'Revizie', value: 'revizie'},
                {label: 'Altul', value: 'altul'}
            ]
        }
    },
    details: {
        type: String,
        label: 'Detalii',
        optional: true
    },
    price: {
        type: Number,
        min: 0,
        label: 'Pret'
    },
    carId: {
        type: String,
        optional: true
    },
    date: {
        type: Date,
        label: 'Data operatiunii'
    }
}));