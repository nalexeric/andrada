import {Cars, Ops} from '/imports/collections';


/*
    Sintaxa pentru find este
    Collection.find(
        {
            //filtre precum
            field: 'valoare',

        },
        {
            //optiuni precum
            sort: {'fieldulDeSortat': 1 sau -1 in functie de ordine}
        }
    )

 */

// Am dat functiei de publish doi parametri, filter si options, pentru a fi foarte flexibila
// Valoarea default pentru fiecare din cei doi parametri este {}, adica un obiect gol, astfel incat daca apelam functia de subscribe fara niciun parametru,
// aceasta va returna toate obiectele.
// Aceasta scriere ne permite insa sa adaugam valori pentru fiecare din cei doi parametri atunci cand apelam functia, inlocuind astfel valorile default
Meteor.publish('cars.find', function (filter = {}, options = {}) {
    return Cars.find(filter, options);
});

Meteor.publish('ops.find', function (filter = {}, options = {}) {
    return Ops.find(filter, options);
});