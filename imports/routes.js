import '/imports/ui/layout';
import '/imports/ui/cars_list';
import '/imports/ui/cars_details';

FlowRouter.route('/', {
    name: 'home',
    action() {
        BlazeLayout.render('layout', {
            main: 'cars_list',
        });
    }
});

FlowRouter.route('/cars/:id', {
    name: 'cars.details',
    action() {
        BlazeLayout.render('layout', {
            main: 'cars_details',
        });
    }
});